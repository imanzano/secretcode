import { createSlice } from '@reduxjs/toolkit'

export const initialState = {
    loading: false,
    hasErrors: false,
    board: [],
    gameId: ""
}

const boardSlice = createSlice({
    name: 'secretboard',
    initialState,
    reducers: {
        getBoard :(state, { payload } ) => {
            //state.loading = true
            state.gameId = payload
        },
        getBoardSuccess: (state,  { payload } ) => {
            state.board = payload
            state.loading = false
            state.hasErrors = false
        },
        getBoardFailure: state => {
            state.loading = false
            state.hasErrors = true
        },
    },
})

export const {
    getBoard,
    getBoardSuccess,
    getBoardFailure,
} = boardSlice.actions
export const boardSelector = state => state.secretboard
export default boardSlice.reducer

export function fetchBoard(gameId) {
    return async dispatch => {
        dispatch(getBoard(gameId))

        try {
            const response = await fetch(
                `/secretcode/services/secretcode/game/${gameId}/secret`
            )
            const data = await response.json()
            console.log("lecko " + JSON.stringify(data) )
            dispatch(getBoardSuccess(data))

        } catch (error) {
            console.log(error)
            dispatch(getBoardFailure())
        }
    }
}

