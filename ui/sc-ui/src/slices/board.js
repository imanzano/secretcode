import { createSlice } from '@reduxjs/toolkit'

export const initialState = {
    loading: false,
    hasErrors: false,
    board: [],
    gameId: ""
}

const boardSlice = createSlice({
    name: 'board',
    initialState,
    reducers: {
        getBoard :(state, { payload } ) => {
            state.loading = true
            state.gameId = payload
        },
        getBoardSuccess: (state,  { payload } ) => {
            state.board = payload
            state.loading = false
            state.hasErrors = false
        },
        getBoardFailure: state => {
            state.loading = false
            state.hasErrors = true
        },
    },
})

export const {
    getBoard,
    getBoardSuccess,
    getBoardFailure,
} = boardSlice.actions
export const boardSelector = state => state.board
export default boardSlice.reducer

export function fetchBoard(gameId) {
    return async dispatch => {
        console.log ("LECKO " + gameId)
        dispatch(getBoard(gameId))

        try {
            const response = await fetch(
                `/secretcode/services/secretcode/game/${gameId}`
            )
            const data = await response.json()
            dispatch(getBoardSuccess(data))

        } catch (error) {
            console.log(error)
            dispatch(getBoardFailure())
        }
    }
}

export function showCard(gameId, cardId) {
    return async dispatch => {
        console.log("LECKO showCard " + gameId + " " + cardId)

        // dispatch(getBoard(gameId))

        try {
            const requestOptions = {
                method: 'POST',
                headers: { 'Content-Type': 'application/json' }
            };
            const response = await fetch(
                `/secretcode/services/secretcode/game/${gameId}/showCard/${cardId}`,
                requestOptions
            )
            const data = await response.json()
            dispatch(getBoardSuccess( data))
        } catch (error) {
            console.log(error)
            dispatch(getBoardFailure())
        }
    }
}