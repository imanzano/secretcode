import { createSlice } from '@reduxjs/toolkit'

export const initialState = {
    gameId: "",
}

const boardSlice = createSlice({
    name: 'home',
    initialState,
    reducers: {
        setGameFromPayload :(state, { payload } ) => {
            state.gameId = payload.first
        },
        setGame :(state, { payload } ) => {
            state.gameId = payload
        }
    },
})

export const {
    setGame,
    setGameFromPayload
} = boardSlice.actions
export const homeSelector = state => state.home
export default boardSlice.reducer



export function createGame() {
    return async dispatch => {

        try {
            const requestOptions = {
                method: 'POST',
                headers: { 'Content-Type': 'application/json' }
            };
            const response = await fetch(
                `/secretcode/services/secretcode/game`,
                requestOptions
            )
            const data = await response.json()
            dispatch(setGameFromPayload( data))
        } catch (error) {
            console.log(error)
        }
    }
}