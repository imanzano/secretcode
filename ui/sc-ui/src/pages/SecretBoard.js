import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'

import { Button, Badge} from 'react-bootstrap';
import { fetchBoard, boardSelector } from '../slices/secretboard'

import If from "../components/If";
import Table from "react-bootstrap/Table";

const SecretBoardPage = ({ match }) => {
    const dispatch = useDispatch()
    const {
        gameId: gameId,
        board: board,
        loading: boardLoading,
        hasErrors: boardHasErrors,
    } = useSelector(boardSelector)

    useEffect(() => {
        const { id } = match.params

        dispatch(fetchBoard(id))
    }, [dispatch, match])

    const buildCell = (value) => {


        if (value === undefined) return <div></div>

        let card = value.card
        let hidden = value.hidden

        return <div>

            <If condition={card.color == "BLUE"}>
                <div>
                    <If condition={hidden}>
                        <Badge pill variant="primary">
                            {card.word.toUpperCase()}
                        </Badge>

                    </If>
                    <If condition={!hidden}>
                        <Badge pill variant="primary">
                            {"       "}
                        </Badge>

                    </If>
                </div>
            </If>
            <If condition={card.color == "RED"}>
                <div>
                    <If condition={hidden}>
                        <Badge pill variant="danger">
                            {card.word.toUpperCase()}
                        </Badge>

                    </If>
                    <If condition={!hidden}>
                        <Badge pill variant="danger">
                            {"       "}
                        </Badge>

                    </If>
                </div>
            </If>
            <If condition={card.color == "YELLOW"}>
                <div>
                    <If condition={hidden}>
                        <Badge pill variant="warning">
                            {card.word.toUpperCase()}
                        </Badge>

                    </If>
                    <If condition={!hidden}>
                        <Badge pill variant="warning">
                            {" "}
                        </Badge>

                    </If>
                </div>
            </If>
            <If condition={card.color == "BLACK"}>
                <div>
                        <Badge pill variant="dark">
                            {card.word.toUpperCase()}
                        </Badge>
                </div>
            </If>
        </div>

    }

    const renderBoard = () => {
        if (boardLoading) return <p>Loading board...</p>
        if (boardHasErrors) return <p>Unable to display board.</p>
        if (board.size === 0) return <div></div>
        console.log("!!!!" + board)
        console.log(board[0])
        return <Table striped bordered hover>
            <tbody>
                <tr>
                    <td>{buildCell(board[0])}</td>
                    <td>{buildCell(board[1])}</td>
                    <td>{buildCell(board[2])}</td>
                    <td>{buildCell(board[3])}</td>
                    <td>{buildCell(board[4])}</td>
                </tr>
                <tr>
                    <td>{buildCell(board[5])}</td>
                    <td>{buildCell(board[6])}</td>
                    <td>{buildCell(board[7])}</td>
                    <td>{buildCell(board[8])}</td>
                    <td>{buildCell(board[9])}</td>
                </tr>
                <tr>
                    <td>{buildCell(board[10])}</td>
                    <td>{buildCell(board[11])}</td>
                    <td>{buildCell(board[12])}</td>
                    <td>{buildCell(board[13])}</td>
                    <td>{buildCell(board[14])}</td>
                </tr>
                <tr>
                    <td>{buildCell(board[15])}</td>
                    <td>{buildCell(board[16])}</td>
                    <td>{buildCell(board[17])}</td>
                    <td>{buildCell(board[18])}</td>
                    <td>{buildCell(board[19])}</td>
                </tr>
                <tr>
                    <td>{buildCell(board[20])}</td>
                    <td>{buildCell(board[21])}</td>
                    <td>{buildCell(board[22])}</td>
                    <td>{buildCell(board[23])}</td>
                    <td>{buildCell(board[24])}</td>
                </tr>
            </tbody>
        </Table>


    }

    return (
        <section>
            <h2>Juego: {gameId}</h2>
            {renderBoard()}
            <div> <Button variant="info" onClick = {() => {
                //const { dispatch } = this.props;
                dispatch(fetchBoard(gameId));
            }}>Refrescar</Button></div>
        </section>
    )
}

export default SecretBoardPage
