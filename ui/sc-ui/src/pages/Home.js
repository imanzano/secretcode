import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'

import { Button, Badge} from 'react-bootstrap';
import { createGame, homeSelector, setGame} from '../slices/home'


import FormGroup from "react-bootstrap/FormGroup";
import FormControl from "react-bootstrap/FormControl";
import {Link} from "react-router-dom";
import OverlayTrigger from "react-bootstrap/OverlayTrigger";
import Tooltip from "react-bootstrap/Tooltip";

const Home = ({ match }) => {
    const dispatch = useDispatch()
    const {
        gameId: gameId
    } = useSelector(homeSelector)


    return (
        <section>
            <h2>Código secreto</h2>
            <Button variant="primary" onClick = {() => {
                //const { dispatch } = this.props;
                dispatch(createGame());
            }}>Nuevo Juego</Button>
            <br/>
            <FormGroup
                controlId="formBasicText"
            >
                <FormControl
                    type="text"
                    value={gameId}
                    placeholder="Nro de Juego"
                    onChange={(e) => {dispatch(setGame(e.target.value))}}
                />
                <FormControl.Feedback />
                {/*<HelpBlock>Validation is based on string length.</HelpBlock>*/}
            </FormGroup>
            <Link to={`/game/${gameId}`} className="button">
                Board
            </Link> { ' '}
            <Link to={`/game/${gameId}/secret`} className="button">
                Secret Board
            </Link>
        </section>
    )
}

export default Home
