import React from 'react'
import { Link } from 'react-router-dom'

export const Navbar = () => (
    <nav>
        <section>
            <Link to="/">Home</Link>
        </section>
    </nav>
)
